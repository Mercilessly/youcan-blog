from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User
from django.views.generic.edit import FormView
from django.views.generic import TemplateView,ListView
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from PIL import Image
from .forms import FormularioLogin
from .models import Pais, Usuario, Posting, Comment
from random import randint
from datetime import datetime


def home(request):
    if request.POST:
        id_post = str(randint(1,100))
        imagen = request.FILES.get("foto")
        if imagen:
            imagen = imagen
        texto = request.POST.get("texto",False)
        fecha_post = datetime.today()
        usuario = request.user
        u = Usuario.objects.get(usuario=usuario)
        p = Posting (id_post=id_post, imagen= imagen, texto= texto, fecha_post=fecha_post, usuario=u)
        p.save()
    p = Posting.objects.all().reverse()
    context= {'Posting':p,}
    return render(request, 'home.html',context)

def DetailPost(request,id_post):
    post = Posting.objects.get(id_post = id_post)
    if request.POST:
        id_post = post
        comment = request.POST.get("comment",False)
        fecha_comment = datetime.today()
        usuario = request.user
        u = Usuario.objects.get(usuario=usuario)
        p = Comment (id_post=id_post, comment=comment, fecha_comment=fecha_comment, usuario=u)
        p.save()
    post = Posting.objects.get(id_post = id_post)
    comments = Comment.objects.filter(id_post=id_post).reverse()
    context = {'post': post, 'comments': comments}
    return render(request, 'posting.html',context)




def Account(request):
    usuario = request.user
    usuario = Usuario.objects.get(usuario=usuario)
    context= {'Usuario': Usuario.objects.filter(usuario=usuario),'Posting':Posting.objects.filter(usuario=usuario).reverse()}
    return render(request, 'account.html',context)

        
def UpdateAccount(request,usuario):
    u = Usuario.objects.get(usuario=usuario)
    context= {'cuenta':u,'Pais': Pais.objects.all()}
    if request.POST:
        u.nombre = request.POST.get("nombre",False)
        u.apellido = request.POST.get("apellido",False)
        u.email = request.POST.get("email",False)
        u.facebook = request.POST.get("facebook",False)
        u.twitter = request.POST.get("twitter",False)
        u.instagram = request.POST.get("instagram",False)
        id_pais = request.POST.get('id_pais')
        p = Pais.objects.get(id_pais=id_pais)
        u.id_pais = p
        foto = request.FILES.get("foto",False)
        if foto:
            u.imagen = foto
        u.save()
        return redirect('account')
    return render(request, 'updateaccount.html',context)


def register(request):
    if request.POST:
        nombre = request.POST.get('nombre', True)
        apellido = request.POST.get('apellido', True)
        usuario = request.POST.get('usuario', True)
        password1 = request.POST.get('password1', True)
        password2 = request.POST.get('password2', True)
        facebook = ''
        twitter = ''
        instagram = ''
        email = request.POST.get('email', True)
        id_pais = request.POST.get('id_pais')
        imagen = ("fotos_perfil/Perfil-Predeterminado.png")
        p = Pais.objects.get(id_pais=id_pais)

        v = Usuario(nombre=nombre, apellido=apellido, usuario=usuario,
                    password1=password1, password2=password2, email=email, id_pais=p,imagen=imagen,
                    facebook =facebook, twitter=twitter, instagram=instagram)

        if(password1 == password2):
            u = User.objects.create_user(
                username=usuario, first_name=nombre, last_name=apellido, email=email, password=password1)
            u.save(),
            v.save()
            return redirect('login')
    context = {'Pais': Pais.objects.all()}
    return render(request, 'register.html', context)

class Login(FormView):
    template_name = 'login.html'
    form_class = FormularioLogin
    success_url = reverse_lazy('account')

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(Login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(Login, self).form_valid(form)


def logoutUsuario(request):
    logout(request)
    return HttpResponseRedirect('/accounts/login/')

