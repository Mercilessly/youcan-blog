from django.contrib import admin
from .models import Usuario,Pais,Posting,Comment
# Register your models here.
admin.site.register(Usuario)
admin.site.register(Pais)
admin.site.register(Posting)
admin.site.register(Comment)