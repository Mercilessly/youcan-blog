# Generated by Django 2.2.6 on 2019-11-16 20:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='facebook',
            field=models.URLField(null=True),
        ),
        migrations.AddField(
            model_name='usuario',
            name='instagram',
            field=models.URLField(null=True),
        ),
        migrations.AddField(
            model_name='usuario',
            name='twitter',
            field=models.URLField(null=True),
        ),
    ]
