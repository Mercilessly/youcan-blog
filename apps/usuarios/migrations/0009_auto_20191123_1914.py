# Generated by Django 2.2.6 on 2019-11-23 22:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0008_auto_20191123_1824'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ['fecha_comment'], 'verbose_name': 'Comment', 'verbose_name_plural': 'Comentarios'},
        ),
        migrations.AlterModelOptions(
            name='posting',
            options={'ordering': ['fecha_post'], 'verbose_name': 'Posting', 'verbose_name_plural': 'Posteos'},
        ),
    ]
