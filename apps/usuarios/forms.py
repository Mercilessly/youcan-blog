from django.contrib.auth.forms import AuthenticationForm
from django import forms
from .models import Usuario

class FormularioLogin(AuthenticationForm):
    def __init__(self,*args,**kwargs):
        super(FormularioLogin,self).__init__(*args,**kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['username'].widget.attrs['placeholder'] = 'Nombre de Usuario'
        self.fields['password'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['placeholder'] = 'Contraseña'

class CuentaForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ['usuario','nombre','email','apellido','id_pais','facebook','instagram','twitter','imagen']