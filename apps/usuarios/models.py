from django.db import models
# Create your models here.


class Pais(models.Model):
    id_pais = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False, blank=False)

    class Meta:
        verbose_name = 'Pais'
        verbose_name_plural = 'Paises'
        ordering = ['nombre']

    def __str__(self):
        return self.nombre


class Usuario(models.Model):
    usuario = models.CharField(primary_key=True, max_length=50)
    password1 = models.CharField(max_length=15, null=False, blank=False)
    password2 = models.CharField(max_length=15, null=False, blank=False)
    nombre = models.CharField(max_length=35, null=False, blank=False)
    apellido = models.CharField(max_length=35, null=False, blank=False)
    email = models.EmailField(max_length=50, null=False, blank=False)
    id_pais = models.ForeignKey(Pais, blank=False, on_delete=models.CASCADE)
    facebook = models.URLField(null=True)
    instagram = models.URLField(null=True)
    twitter = models.URLField(null=True)
    imagen = models.ImageField(upload_to="fotos_perfil", null=True)

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'
        ordering = ['usuario']

    def __str__(self):
        return self.usuario


class Posting(models.Model):
    id_post = models.CharField(primary_key=True,max_length=10)
    imagen = models.ImageField(upload_to="fotos_posting", null=True)
    texto = models.CharField(max_length=100, null=False)
    fecha_post = models.DateTimeField(null=False)
    usuario = models.ForeignKey(Usuario, null=False, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Posting'
        verbose_name_plural = 'Posteos'
        ordering = ['fecha_post']

    def __str__(self):
        return self.id_post


class Comment(models.Model):
    id_post = models.ForeignKey(Posting, null=False, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, null=False, on_delete=models.CASCADE)
    comment = models.TextField(max_length=200, null=False)
    fecha_comment = models.DateTimeField(null=False)
    

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comentarios'
        ordering = ['fecha_comment']
    
    def __str__(self):
        return '{}'.format(self.id_post)
