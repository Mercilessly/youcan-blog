$(document).ready(function(){
    $("#Posting").validate({
        errorClass: "is-invalid",
        rules:{
            texto:{
                required:true,
            },
        },
        messages:{
            texto:{
                required:"Para postear al menos se necesita una descripcion"
            },
        }
    })
})

$("#Posting").submit(function(){
    if($("#Posting").valid()){
        console.log("valido")
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Oh no.!',
            text: 'Has cometido un error OnO',
          })
    }
    return false
})
