$(document).ready(function(){
    $("#Comment").validate({
        errorClass: "is-invalid",
        rules:{
            comment:{
                required:true,
            },
        },
        messages:{
            comment:{
                required:"no puedes dejar el comentario en blanco"
            },
        }
    })
})

$("#Comment").submit(function(){
    if($("#Comment").valid()){
        console.log("valido")
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Oh no.!',
            text: 'Has cometido un error OnO',
          })
    }
    return false
})