$(document).ready(function () {
    $('#modificar').click(function() {
        $('#facebook').val('');
        $('#instagram').val('');
        $('#twitter').val('');
    });
});


$(document).ready(function () {
    $("#update").validate({
        errorClass: "is-invalid",
        rules: {
            nombre: {
                required: true,
            },
            apellido: {
                required: true,
            },
            usuario: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            id_pais: {
                required: true,
            },
        },
        messages: {

            nombre: {
                required: "El campo requerido",
            },
            apellido: {
                required: "El campo requerido",
            },
            usuario: {
                required: "El campo requerido",
            },
            email: {
                required: "El campo requerido",
                email: "Ingresar un Correo Electronico valido",
            },
            id_pais: {
                required: "El campo requerido",
            },

        },
    })
})


$("#update").submit(function () {
    if ($("#update").valid()) {
        return true
    }
    else {
        Swal.fire({
            type: 'error',
            title: 'Oh no.!',
            text: 'Has cometido un error :c',
        })
    }
    return false
})