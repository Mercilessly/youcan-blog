$(document).ready(function () {
    $("#Register").validate({
        errorClass: "is-invalid",
        rules: {
            nombre: {
                required: true,
            },
            apellido: {
                required: true,
            },
            usuario: {
                required: true,
            },
            password1: {
                required: true,
                error: true,
            },
            password2: {
                required: true,
                error: true,
            },
            email: {
                required: true,
                email: true,
            },
            id_pais: {
                required: true,
            },
        },
        messages: {

            nombre: {
                required: "El campo requerido",
            },
            apellido: {
                required: "El campo requerido",
            },
            usuario: {
                required: "El campo requerido",
            },
            password1: {
                required: "El campo requerido",
            },
            password2: {
                required: "El campo requerido",
            },
            email: {
                required: "El campo requerido",
                email: "Ingresar un Correo Electronico valido",
            },
            id_pais: {
                required: "El campo requerido",
            },

        },
    })

    $("#password1").on("keyup",function() {
        var password1 = $("#password1").val()
        var password2 = $("#password2").val()
        if (password1 != password2) {
            $("#mensaje").text("Las contraseñas no coinciden").css("color", "red");
            return false;
        } else {
            $("#mensaje").text("Las contraseñas coinciden").css("color", "green")
            return true;
        };
    });

    $("#password2").on("keyup", function ()  {
        var password1 = $("#password1").val()
        var password2 = $("#password2").val()
        if (password1 != password2) {
            $("#mensaje").text("Las contraseñas no coinciden").css("color", "red");
            return false;
        } else {
            $("#mensaje").text("Las contraseñas coinciden").css("color", "green")
            return true;
        };
    });
})


$("#Register").submit(function () {
    if ($("#Register").valid()){
        return true
    }
    else {
        Swal.fire({
            type: 'error',
            title: 'Oh no.!',
            text: 'Has cometido un error OnO',
        })
    }
    return false
})