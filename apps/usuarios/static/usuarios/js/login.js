$(document).ready(function(){
    $("#Login").validate({
        errorClass: "is-invalid",
        rules:{
            username:{
                required:true,
            },
            password:{
                required: true,
            }
        },
        messages:{
            username:{
                required:"El campo Usuario es requerido"
            },
            password:{
                required: "El campo Contraseña es requerido"
            }
        }
    })
})

$("#Login").submit(function(){
    if($("#Login").valid()){
        console.log("valido")
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Oh no.!',
            text: 'Has cometido un error :C',
          })
    }
    return false
})





