"""YouCanBlog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from apps.usuarios.views import register,home,DetailPost,Account,UpdateAccount,Login,logoutUsuario
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/',register,name='register'),
    path('',home,name='home'), 
    path('account/',login_required(Account),name='account'),
    path('update/<str:usuario>/',login_required(UpdateAccount),name='update'),
    path('post/<str:id_post>/',DetailPost,name='post'),
    path('accounts/login/',Login.as_view(),name= 'login'),
    path('logout/',login_required(logoutUsuario),name='logout'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
